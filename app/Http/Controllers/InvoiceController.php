<?php

namespace App\Http\Controllers;

use App\Models\Invoice;
use App\Models\Invoice_item;
use App\Models\Invoice_item_master;
use Illuminate\Http\Request;

class InvoiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Invoice::where('isDeleted', 0)->get();

        return view('invoice.invoice')->with('data', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = Invoice_item_master::all();
        $data = ['itemMaster' => $data, 'url' => '/invoice'];

        return view("invoice.addInvoice")->with('data', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'name' => 'required',
            'invoiceItems' => 'required',
        ]);

        $invoiceItems = explode(",", $request->invoiceItems);
        $name = $request->name;
        $invDb = new Invoice();
        $invDb->name = $name;
        $invDb->save();
        $invDb->id;

        foreach ($invoiceItems as $it) {
            $itemDb = new Invoice_item();
            $itemDb->invoiceId =  $invDb->id;
            $itemDb->itemId  = $it;
            $itemDb->save();
        }

        return redirect('/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $invoiceData = Invoice::find($id);
        $itemMaster = Invoice_item_master::all();
        $selecredItemData = Invoice_item::leftJoin('invoice_item_masters', function ($join) {
            $join->on('invoice_items.itemId', '=', 'invoice_item_masters.id');
        })
            ->where('invoice_items.invoiceId', $id)
            ->where('invoice_items.isDeleted', 0)
            ->where('invoice_item_masters.isDeleted', 0)
            ->get([
                'invoice_items.id',
                'invoice_items.invoiceId',
                'invoice_items.itemId',
                'invoice_item_masters.itemName',
            ]);


        $temp = [];
        foreach ($selecredItemData as $dt) {
            array_push($temp, $dt['itemId']);
        }
        $oldItems = implode(",", $temp);


        $data = [
            'invoiceData' => $invoiceData, 'selecredItemData' => $selecredItemData, 'itemMaster' => $itemMaster,
            'oldItems' => $oldItems, 'type' => 'Update Invoice', 'url' => "/invoice/$id", 'method' => 'POST'
        ];



        return view("invoice.addInvoice")->with('data', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validated = $request->validate([
            'name' => 'required',
            'invoiceItems' => 'required',
        ]);

        Invoice::where('id', $id)->update(['isDeleted' => 1]);
        Invoice_item::where('invoiceId', $id)->update(['isDeleted' => 1]);

        $invoiceItems = explode(",", $request->invoiceItems);
        $name = $request->name;
        $invDb = new Invoice();
        $invDb->name = $name;
        $invDb->save();
        $invDb->id;

        foreach ($invoiceItems as $it) {
            $itemDb = new Invoice_item();
            $itemDb->invoiceId =  $invDb->id;
            $itemDb->itemId  = $it;
            $itemDb->save();
        }

        return redirect('/');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        Invoice::where('id', $id)->update(['isDeleted' => 1]);
        Invoice_item::where('invoiceId', $id)->update(['isDeleted' => 1]);
        return redirect('/');
    }
}
