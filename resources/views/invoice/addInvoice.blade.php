@extends('layouts.app')
@section('content')

@if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif


<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js" integrity="sha512-VEd+nq25CkR676O+pLBnDW09R7VQX9Mdiij052gVCp5yVH3jGtH70Ho/UUv4mJDsEdTvqRCFZg0NKGiojGnUCw==" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css" integrity="sha512-3pIirOrwegjM6erE5gPSwkUzO+3cTjpnV9lexlNZqvupR64iZBnOOTiiLPb9M36zpMScbmUNIcHUqKD47M719g==" crossorigin="anonymous" />
<div class="container ">
    <br>
    <div class="card">
        <div class="card-header ">

            <h4 align="center"><b>Add Invoices</b></h4>

        </div>
        <div class="card-body">
            <form action="{{$data['url']}}" method="<?= $data['method'] ?? "POST" ?>">
                @csrf
                @if(isset($data['method']))
                @method('PUT')
                @endif



                <div class="form-group row mt-2">
                    <div class="col-md-2">
                        <label for="exampleFormControlInput1">Name :</label>
                    </div>
                    <div class="col-md-4">
                        <input type="name" name="name" class="form-control" placeholder="Name" value="<?= $data['invoiceData']['name'] ?? '' ?>">
                    </div>
                </div>

                <div class="form-group row mt-2">
                    <div class="col-md-2">
                        <label for="exampleFormControlInput1">Items :</label>
                    </div>
                    <div class="col-md-4">

                        @foreach($data['itemMaster'] as $dt)
                        <button type="button" class="btn btn-primary" onclick="addItem('{{$dt->id}}','{{$dt->itemName}}')">{{$dt->itemName}} <i class="fa fa-plus" aria-hidden="true"></i>
                        </button>
                        @endforeach
                    </div>
                </div>

                <div class="form-group row mt-2">
                    <div class="col-md-2">
                        <label for="exampleFormControlInput1">Added Items :</label>
                    </div>
                    <div class="col-md-4">
                        <span id="itemAdded">
                            @if(isset($data['selecredItemData']))
                            @foreach($data['selecredItemData'] as $selectedDt)


                            <button id="item_{{$selectedDt['itemId']}}" type="button" class="btn btn-success mb-2 ml-2" onclick="removeItem({{$selectedDt['itemId']}})">{{$selectedDt['itemName']}}<i class="fa fa-times" aria-hidden="true"></i>

                                @endforeach
                                @endif
                        </span>
                    </div>
                </div>
                <input type="hidden" name="invoiceItems" value="<?= $data['oldItems'] ?? '' ?>">

                <div class="form-group row mt-5">
                    <div class="col-md-2">

                    </div>
                    <div class="col-md-4">
                        <button class="btn btn-success">
                            <?= $data['type'] ?? "Save Invoice"; ?>
                        </button>
                    </div>
                </div>


            </form>

        </div>
    </div>
</div>
<script>
    var itemIds = [];

    $(document).ready(function() {
        var string = $('input[name=invoiceItems]').val()
        console.log("string=", string)
        if (string)
            itemIds = string.split(',');

    });


    function addItem(itemId, itemName) {
        console.log("itemId=", itemId)
        if (itemIds.includes(itemId)) {
            toastr.error('Item already added');
        } else {
            itemIds.push(itemId)
            html = ' <button id="item_' + itemId + '" type="button" class="btn btn-success mb-2" onclick="removeItem(' + itemId + ')" >' + itemName + ' <i class="fa fa-times" aria-hidden="true"></i>';

            $("#itemAdded").append(html)

            $("input[name=invoiceItems]").val(itemIds);
            console.log("itemIds=", itemIds)
        }



    }

    function removeItem(id) {
        console.log("removeItem=", id);
        $("#item_" + id).remove();
        var removeItem = id;
        itemIds = jQuery.grep(itemIds, function(value) {
            return value != removeItem;
        });
        $("input[name=invoiceItems]").val(itemIds);
        console.log("itemIds=", itemIds)
    }
</script>

@endsection