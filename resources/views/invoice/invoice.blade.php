@extends('layouts.app')
@section('content')


<div class="container ">
    <br>
    <div class="card">
        <div class="card-header ">

            <h4 align="center"><b>Invoices</b></h4>

        </div>
        <div class="card-body">
            <div align="right">
                <a href="invoice/create" class="btn btn-primary">Add Invoice</a>
            </div>
            <table class="table table-bordered mt-2">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">Sr. No.</th>
                        <th scope="col">Name</th>
                        <th scope="col">Date</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @if(isset($data))
                    @foreach($data as $dt)
                    <tr>
                        <th scope="row">{{$dt->id}}</th>
                        <td>{{$dt->name}}</td>
                        <td>{{$dt->created_at}}</td>
                        <td>
                            <div class="row">
                                <div class="col-md-2">
                                    <a href="/invoice/{{$dt->id}}/edit" class="btn btn-primary ">Edit</a>
                                </div>
                                <div class="col-md-2">
                                    <form id="/invoice/{{$dt->id}}" + action="{{route('invoice.destroy', $dt->id)}}" method="post">
                                        @csrf @method('DELETE')
                                        <button type="submit" class="btn btn-danger">Delete</button>
                                    </form>
                                </div>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                    @endif
                </tbody>
            </table>

        </div>
    </div>
</div>


@endsection